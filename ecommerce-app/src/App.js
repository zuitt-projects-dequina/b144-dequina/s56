
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';


/* Components */
import AppNavbar from './components/AppNavBar';

/* Pages */
import Home from './pages/Home';
import Login from './pages/Login';
import SignUp from './pages/SignUp'



function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
        </Switch>
      </Container>

    </Router>
  );
}

export default App;
