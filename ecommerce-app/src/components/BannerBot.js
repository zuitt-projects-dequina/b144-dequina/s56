import { Fragment } from 'react';
import { Row, Col } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import '../App.css';


export default function BannerBot() {
    
    return (
        <Container fluid className='m-4'>
        <Row className="justify-content-md-center">
            <Col id="title-bot" className='col-12 col-md-auto'>
                <h2>New</h2>
                <h2>Release</h2>
            </Col>
            
            
            <Col className='bg-warning col-12 col-md-2'>
                <h1>book 1</h1>
            </Col>
            <Col className='bg-primary col-12 col-md-2'>
                <h1>book 2</h1>
            </Col>
            <Col className='bg-info col-12 col-md-2'>
                <h1>book 3</h1>
            </Col>
                    
            
        </Row>
        </Container>
        
    );
};