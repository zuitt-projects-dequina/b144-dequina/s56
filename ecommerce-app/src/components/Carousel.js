import { Carousel } from "react-bootstrap";
import '../App.css';


export default function CarouselBooks() {
    return (
        <Carousel fade>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/children-kids-book-cover-template-design-2dc7dd4615b1d4b5dce836e7113e9268.jpg?ts=1636989583"
                    alt="First slide"
                    
                />
                <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/prismatic-romantic-novel-book-cover-template-design-3ccf99c330ac08940e85222819525b47.jpg?ts=1636983226"
                    alt="Second slide"
                />

                <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/vintage-novel-kindle-book-cover-design-template-255f294cd4675f104c5272471013477a.jpg?ts=1636984087"
                    alt="Third slide"
                />

                <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
};