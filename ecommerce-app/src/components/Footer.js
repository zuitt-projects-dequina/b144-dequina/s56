import React from "react"

export default function Footer() {
    return (
        <footer className="page-footer font-small pt-4">
            

            <div className="footer-copyright text-center py-3">
                <span>&#169; All Rights Reserve 2022</span>
            </div>

        </footer>
    );
};
