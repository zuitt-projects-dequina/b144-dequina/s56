import React from "react";
import { MDBCol } from "mdbreact";

const SearchBar = () => {
  return (
    <MDBCol md="auto">
      <input className="form-control my-3" type="text" placeholder="Search" aria-label="Search" />
    </MDBCol>
  );
}

export default SearchBar;