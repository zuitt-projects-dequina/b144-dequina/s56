import { Fragment } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import BannerTop from '../components/BannerTop';
import BannerBot from '../components/BannerBot';
import Footer from '../components/Footer';



export default function Home() {

    return (
        <Fragment>
            <Row id="landing-container" className='my-5'>

                <Row className='my-5'>
                    <BannerTop />
                </Row>

                <Row className='my-5'>
                    <BannerBot />
                </Row>
            </Row>
            <Footer />
        </Fragment>
    )
}