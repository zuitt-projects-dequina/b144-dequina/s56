import { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import Footer from "../components/Footer";
import { NavLink } from "react-router-dom";
import { Nav } from "react-bootstrap";
import Signup from './SignUp';

export default function Login() {

    //const { user, setUser } = useContext(UserContext);

    // State hooks for email and password
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // State to determine wether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    return (
        <div>
        <Container id="login-container" className="col-12 col-md-3">
            <h2>Login</h2>

            <Form>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {/* Conditionally render submit button based on isActive state */}
                {isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Login
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled >
                        Login
                    </Button>
                }

                <Form.Group className="mt-3 text-center" controlId="password">
                    <Form.Label>
                        Dont have an account? 
                        <Nav.Link as={NavLink} to="/signup">Sign up</Nav.Link>
                    </Form.Label>

                </Form.Group>

            </Form>
            
        </Container>
        <Footer />
        </div>
    );
}