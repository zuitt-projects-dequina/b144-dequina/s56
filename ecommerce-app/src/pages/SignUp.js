import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';
import { NavLink } from "react-router-dom";
import { Nav } from "react-bootstrap";


export default function SignUp() {

    // userData
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [gender, setGender] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    return (
        <div>
        <Container id="signup-container" className="col-12 col-md-3">
            <h2>Sign up</h2>
            <hr></hr>
        <Form>
            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="age">
                <Form.Label>Age</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter Age"
                    value={age}
                    onChange={e => setAge(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="gender">
                <Form.Label>Gender</Form.Label>
                <Form.Control
                    as="select"
                    placeholder="Enter Gender (Male or Female)"
                    value={gender}
                    onChange={e => setGender(e.target.value)}
                    required
                >

                    <option type="text" value="" disabled>Select a Gender</option>
                    <option type="text" value="male">Male</option>
                    <option type="text" value="female">Female</option>
                    <option type="text" value="other">Other</option>
                </Form.Control>
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter Mobile Number"
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>


            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>

            <Form.Group className="mt-3 text-center" controlId="password">
                    <Form.Label>
                        Already have an account? 
                        <Nav.Link as={NavLink} to="/login">Log in</Nav.Link>
                    </Form.Label>
            </Form.Group>
        </Form>
        </Container>
        <Footer />
        </div>
    );
}